import {DataType} from "../components/regas-table/model/TableOptions.model";

export const FILTERABLE_TYPES: DataType[] = [DataType.String, DataType.Number, DataType.Date, DataType.Boolean];
export const SORTABLE_TYPES: DataType[] = [DataType.String, DataType.Number, DataType.Date, DataType.Boolean];
