import {Column, DataType} from "../components/regas-table/model/TableOptions.model";
import {FILTERABLE_TYPES} from "./options";



export const processRows = (data: any[], columns: Column[]) => data.map((row) => proccesRow(row, columns));
export const proccesRow = (row: any, columns: Column[]) => {
  return columns.map((column: Column) => generateDisplayValue(row, column))
};

export const generateDisplayValue = (row: any, column: Column) => {
  switch (column.type) {
    case DataType.Date:
      return Intl.DateTimeFormat("nl-NL", column.dateFormat ?? {
        year: "numeric",
        month: "2-digit",
        day: "2-digit"
      }).format(new Date(row[column.property]));
    case DataType.Boolean:
      return row[column.property] ? column.truthyValue : column.falsyValue;
    case DataType.Number:
      return !!column.precision
        ? Number(row[column.property]).toFixed(column.precision)
        : String(row[column.property]);
    default:
      return row[column.property];
  }
}

export const applySort = (data: any[], column: Column, ascending = true) => {
  if (!column) return data;
  const sorted =  data.sort(((a, b) => {
    if (column.type === DataType.String) {
      return sortStrings(a[column.property], b[column.property]);
    }
    if (column.type === DataType.Number) {
      return sortNumbers(a[column.property], b[column.property]);
    }
    if (column.type === DataType.Date) {
      return sortDates(a[column.property], b[column.property]);
    }
  }));
  return ascending ? sorted : sorted.reverse();
};

export const applyFilter = (searchWord: string, data: any[], columns: Column[]) => {
  if (searchWord.length) return data;
  return data.filter(row => {
    for (let column of columns){
      if (FILTERABLE_TYPES.includes(column.type) &&
        (row[column.property] as string)
          .toLowerCase()
          .includes(searchWord.toLowerCase())
      ){
        return true;
      }
    }
    return false;
  })
};

export const applyPagination = (page: number, stepSize: number, data: any[]) => data.slice((page-1) * stepSize, page * stepSize);

export const sortStrings = (first: string, second: string): number => {
  first  = String(first);
  second  = String(second);
  if (first < second) {
    return -1;
  }
  if (first > second) {
    return 1;
  }
  return 0;
};
export const sortNumbers = (first: number, second: number): number => first - second;
export const sortDates = (first: Date, second: Date): number => {
  first = new Date(first);
  second = new Date(second);
  return sortNumbers(first.valueOf(), second.valueOf())
};
