import {Component, h, Event, EventEmitter, Prop} from '@stencil/core';
import {ButtonColumn, Column, DataType, LinkColumn, TextReplacement} from "../regas-table/model/TableOptions.model";
import {generateDisplayValue} from "../../utils/utils";

@Component({
  tag: 'table-row',
  styleUrl: 'table-row.css',
  shadow: true,
})
export class TableRow {

  @Prop() columns: Column[];
  @Prop() data: any;

  @Event() edit: EventEmitter;
  @Event() buttonClick: EventEmitter;

  generateRow() {
    return this.columns.map((column : Column) => {
      switch (column.type) {
        case DataType.Button:
          return this.generateButtonCell(this.data, column);
        case DataType.Link:
          return this.generateLinkCell(this.data, column);
        case DataType.Edit:
          return this.generateEditCell(this.data);
        default:
          return (<td>{ generateDisplayValue(this.data, column) }</td>);
      }
    });
  };


  generateButtonCell(row: any, column: ButtonColumn) {
    let buttonText = column.buttonText;
    column?.buttonTextReplacements?.forEach((replacement: TextReplacement) => {
      const regex = new RegExp(replacement.replace, "g");
      buttonText = buttonText.replace(regex, row[replacement.property])
    });

    const onClick = () => {
      this.buttonClick.emit({
        columnIdentifier: column.dispatchedEventIdentifier,
        entity: row
      })
    };

    return (
      <button onClick={onClick}>
        {buttonText}
      </button>
    );

  };

  generateLinkCell(row: any, column: LinkColumn) {
    let linkText = column.linkText;
    let href = column.href;
    column.linkTextReplacements?.forEach((replacement: TextReplacement) => {
      const regex = new RegExp(replacement.replace, "g");
      linkText = column.linkText.replace(regex, row[replacement.property])
    });

    column?.hrefReplacements?.forEach((replacement: TextReplacement) => {
      const regex = new RegExp(replacement.replace, "g");
      href = href.replace(regex, row[replacement.property])
    });
    return (<a href={href}>{linkText}</a>)
  };

  generateEditCell(row: any) {
    console.log(this);
    const onClick = ()=> {
      this.edit.emit(row);
    };
    return (<td><button onClick={onClick}>edit</button></td>);
  };

  render() {
    return (<tr>
      {this.generateRow()}
    </tr>)
  }
}
