# regas-table

Dit is eigen code

<!-- Auto Generated Below -->


## Properties

| Property       | Attribute | Description | Type           | Default                                                                |
| -------------- | --------- | ----------- | -------------- | ---------------------------------------------------------------------- |
| `columns`      | --        |             | `Column[]`     | `undefined`                                                            |
| `data`         | --        |             | `any[]`        | `undefined`                                                            |
| `tableOptions` | --        |             | `TableOptions` | `{     showCheckBox: false,     filter: true,     truncate: false   }` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
