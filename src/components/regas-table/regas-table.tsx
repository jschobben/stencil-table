import {Component, Event, EventEmitter, h, Prop, State} from '@stencil/core';
import {
  ButtonColumn,
  Column,
  DataType,
  LinkColumn, StepSize,
  TableOptions, TablePagination, TableSort,
  TextReplacement
} from "./model/TableOptions.model";
import {applyPagination, applySort, generateDisplayValue} from "../../utils/utils";

@Component({
  tag: 'regas-table',
  styleUrl: 'regas-table.css',
  shadow: true,
})
export class RegasTable {
  @State() private pagination: TablePagination = {
    page: 1,
    stepSize: 25
  }
  @State() private sort: TableSort;
  @Prop() columns: Column[] = [];
  @Prop() data: any[] = [];
  @Prop() selected: any[] = [];
  @Prop() options: TableOptions = {
    showCheckBox: false,
    filter: true,
    truncate: false
  };

  @Event() edit: EventEmitter;
  @Event() buttonClick: EventEmitter;
  @Event() selectedChange : EventEmitter;


  private onPrevClick(): void {
    if (this.pagination.page !== 1) {
      this.pagination = {
        page: --this.pagination.page,
        stepSize: this.pagination.stepSize
      }
    }
  }

  private onNextClick(): void {
    if (this.pagination.page !== Math.ceil(this.data.length / this.pagination.stepSize)) {
      this.pagination = {
        page: ++this.pagination.page,
        stepSize: this.pagination.stepSize
      }
    }
  }

  private setStepSize(value: string): void {
    this.pagination = {
      page: 1,
      stepSize: Number(value) as StepSize
    }
  }

  private onAllChecked(): void {

    const allSelected = this.selected.length === this.data.length;
    if (allSelected) {
      this.selected = [];
    } else {
      this.selected = this.data.map(d => d[this.options.identifier]);
    }
    this.selectedChange.emit(this.selectedChange);
  }

  private onCheckBoxCheck(id: any) {
    const selectedIndex = this.selected.indexOf(id);
    if (selectedIndex >= 0) {
      this.selected.splice(selectedIndex, 1)
    } else {
      this.selected.push(id);
    }
    this.selectedChange.emit(this.selected);
  }

  private setSort(column: Column) {
    // TODO check (with linda) does the page needs to be rerenderd after

    this.pagination = {
      ...this.pagination,
      page: 1
    };

    if (this.sort?.column.property === column.property) {
      this.sort = {
        column,
        ascending: !this.sort.ascending
      };
    } else {
      this.sort = {
        column,
        ascending: true
      }
    }

  }

  private generateRow(row: any) {
    const tds = this.columns.map((column : Column) => {
      switch (column.type) {
        case DataType.Button:
          return this.generateButtonCell(row, column);
        case DataType.Link:
          return this.generateLinkCell(row, column);
        case DataType.Edit:
          return this.generateEditCell(row);
        default:
          return (<td>{ generateDisplayValue(row, column) }</td>);
      }
    });
    return tds;
  };

  private generateButtonCell(row: any, column: ButtonColumn) {
    let buttonText = column.buttonText;
    column?.buttonTextReplacements?.forEach((replacement: TextReplacement) => {
      const regex = new RegExp(replacement.replace, "g");
      buttonText = buttonText.replace(regex, row[replacement.property])
    });

    const onClick = () => {
      this.buttonClick.emit({
        columnIdentifier: column.dispatchedEventIdentifier,
        entity: row
      })
    };

    return (
      <button onClick={onClick}>
        {buttonText}
      </button>
    );

  };

  private generateLinkCell(row: any, column: LinkColumn) {
    let linkText = column.linkText;
    let href = column.href;
    column.linkTextReplacements?.forEach((replacement: TextReplacement) => {
      const regex = new RegExp(replacement.replace, "g");
      linkText = column.linkText.replace(regex, row[replacement.property])
    });

    column?.hrefReplacements?.forEach((replacement: TextReplacement) => {
      const regex = new RegExp(replacement.replace, "g");
      href = href.replace(regex, row[replacement.property])
    });
    return (<a href={href}>{linkText}</a>)
  };

  private generateEditCell(row: any) {
    const onClick = ()=> {
      this.edit.emit(row);
    };
    return (<td><button onClick={onClick}>edit</button></td>);
  };

  render() {
    const t0 = performance.now()
    const sortedData = applySort(this.data, this.sort?.column, this.sort?.ascending);
    // const displayedData = proccesRow(sortedData, this.columns);
    // const filteredData = applyFilter(this.searchWord, displayedData, this.columns);
    const paginatedData = applyPagination(this.pagination.page, this.pagination.stepSize, sortedData);
    const t1 = performance.now();
    console.log("Call to doSomething took " + (t1 - t0) + " milliseconds.");


    return (
      <div>
        <table>
          <thead>
          <tr>
            {this.options.showCheckBox ? <td><input type="checkbox" onChange={()=> this.onAllChecked()}/></td>: undefined}
            {this.columns?.map((column: Column) => {
              return (<th onClick={() => this.setSort(column)}>{column.label}</th>);
            })}
          </tr>
          </thead>
          <tbody>
          {paginatedData?.map(row => {
            return (
              <tr key={row[this.options.identifier]}>
                {this.options.showCheckBox ? <td><input type="checkbox" onChange={()=> this.onCheckBoxCheck(row[this.options.identifier])} checked={this.selected.includes(row[this.options.identifier])}/></td> : undefined}
                {this.generateRow(row)}
              </tr>
            )
          })}
          </tbody>
        </table>
        <button onClick={() => this.onPrevClick()}>prev</button>
        page {this.pagination.page} of {Math.ceil(this.data.length / this.pagination.stepSize)}
        <button onClick={()=> this.onNextClick() }>next</button>
        <select onChange={event=> {this.setStepSize((event.target as HTMLSelectElement).value)}}>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="75">75</option>
          <option value="100">100</option>
        </select>
      </div>
    );
  }
}
