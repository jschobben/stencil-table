import { newSpecPage } from '@stencil/core/testing';
import { RegasTable } from './regas-table';

describe('regas-table', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [RegasTable],
      html: `<regas-table></regas-table>`,
    });
    expect(page.root).toEqualHtml(`
      <regas-table>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </regas-table>
    `);
  });
});
