import DateTimeFormatOptions = Intl.DateTimeFormatOptions;

export enum DataType {
  String = "string",
  Number = "number",
  Date = "date",
  Button = "button",
  Edit = "edit",
  Link = "link",
  Boolean = "boolean"
}

export interface TextReplacement {
  property: string;
  replace: string;
}

export interface StringColumn extends BaseColumn {
  type: DataType.String;
}

export interface NumberColumn extends BaseColumn {
  type: DataType.Number;
  precision: number;
}

export interface DateColumn extends BaseColumn {
  type: DataType.Date;
  dateFormat: DateTimeFormatOptions;
}

export interface ButtonColumn extends BaseColumn {
  type: DataType.Button;
  dispatchedEventIdentifier: string;
  buttonText: string;
  buttonTextReplacements?: TextReplacement[];
}

export interface EditColumn extends BaseColumn {
  type: DataType.Edit;
}

export interface LinkColumn extends BaseColumn {
  type: DataType.Link;
  linkText: string;
  linkTextReplacements?: TextReplacement[];
  href: string;
  hrefReplacements?: TextReplacement[];
}

export interface BooleanColumn extends BaseColumn {
  type: DataType.Boolean;
  truthyValue: string;
  falsyValue: string;
}

interface BaseColumn {
  property: string;
  label: string;
  type: DataType
}

export interface TableOptions {
  filter?: boolean;
  showCheckBox?: boolean;
  truncate?: boolean;
  identifier?: string;
}

export interface TablePagination {
  stepSize: StepSize;
  page: number;
}

export interface TableSort {
  column: Column;
  ascending: boolean;
}

export type Column = StringColumn |
  NumberColumn |
  DateColumn |
  ButtonColumn |
  EditColumn |
  LinkColumn |
  BooleanColumn;

export type StepSize = 25 | 50 | 75 | 100;

